import React, { FC } from 'react';
import logo from './logo.svg';
import './index.scss';

const commentData = {
  title: "Fake article title.",
  author: "grzm",
  comments: [
    {
      id: 1,
      text: "Example comment here.",
      author: "user2",
      children: [
        {
          id: 2,
          text: "Another example comment text.",
          author: "user3",
          children: [
            {
              id: 3,
              text: "Another example comment text.",
              author: "user4",
              children: []
            }
          ]
        }
      ]
    },
    {
      id: 4,
      text: "Example comment here 2.",
      author: "user5",
      children: []
    }
  ]
}

const CommentComponent:FC<{comment:any, type:string}> = ({ comment }) => {
  const nestedComments = (comment.children || []).map((comment:any) => {
    return <CommentComponent key={comment.id} comment={comment} type="child" />
  })

  return (
    <div style={{"marginLeft": "25px", "marginTop": "10px"}}>
      <div>{comment.text}</div>
      {nestedComments}
    </div>
  )
}

function App() {
  return (
    <div>
      {
        commentData.comments.map((comment) => {
          return (
            <CommentComponent key={comment.id} comment={comment} type=""/>
          )
        })
      }
    </div>
  )
}

export default App