import React, { createContext, useEffect, useState } from 'react'
import IactionProvider from '../../../utils/interfaces/actionProvider'
import IactionContext from '../../../utils/interfaces/actionContext'
import Icomments from '../../../utils/interfaces/comment'
import uuid from 'react-uuid'

export const ActionContext = createContext<Partial<IactionContext>>({});
export const ActionProvider:React.FC<IactionProvider> = (props) => {
  const [replies, setReplies] = useState([])
  const [user, setUser] = useState(false)
  const [editArr, setEdit] = useState([])

  /* setting current user */
  useEffect(() => {
    if (props.currentUser) {
      setUser(true)
    } else {
      setUser(false)
    }
  })

  /* Handles reply or edit feature */
  const handleAction = (id:never, edit:boolean) => {
    edit ? setEdit([...editArr, id]) : setReplies([...replies, id])
  }
  /* Cancel action for posting or editing */
  const handleCancel = (id:string, edit: boolean) => {
    if (edit) {
      const list = [...editArr]
      const newList = list.filter((i) => i !== id)
      setEdit(newList)
    } else if (!edit) {
      const list = [...replies]
      const newList = list.filter((i) => i !== id)
      setReplies(newList)
    }
  }

  /* Post comment to thread */
  const onSubmit = (text: string, parentId: string, child:any) => {
    if (text.length > 0) {
      if (!parentId && !child) {
        props.setComment([
          ...props.comments,
          {
            userId: props.currentUser.userId,
            comId: uuid(),
            avatarUrl: props.currentUser.avatarUrl,
            fullName: props.currentUser.name,
            text: text
          }
        ])
      } else if (parentId && child) {
        let newList: Icomments[] = [];
        newList = [...props.comments];
        const index = newList.findIndex((x) => x.comId === parentId)
        newList[index].replies.push({
          userId: props.currentUser.userId,
          comId: uuid(),
          avatarUrl: props.currentUser.avatarUrl,
          fullName: props.currentUser.name,
          text: text
        })
        props.setComment(newList)
      } else if (parentId && !child) {
        const newList = [...props.comments]
        const index = newList.findIndex((x) => x.comId === parentId)
        const newReplies =
          newList[index].replies === undefined
            ? []
            : [...newList[index].replies]
        newReplies.push({
          userId: props.currentUser.userId,
          comId: uuid(),
          avatarUrl: props.currentUser.avatarUrl,
          fullName: props.currentUser.name,
          text: text
        })
        newList[index].replies = newReplies
        props.setComment(newList)
      }
    }
  }

/* Edit comment */
  const editText = (id:string, text:string, parentId:string) => {
    if (parentId === undefined) {
      const newList = [...props.comments]
      const index = newList.findIndex((x) => x.comId === id)
      newList[index].text = text
      props.setComment(newList)
    } else if (parentId !== undefined) {
      const newList = [...props.comments]
      const index = newList.findIndex((x) => x.comId === parentId)
      const replyIndex = newList[index].replies.findIndex((i) => i.comId === id)
      newList[index].replies[replyIndex].text = text
      props.setComment(newList)
    }
  }

  /* Delete comment */
  const deleteText = (id:string, parentId:string) => {
    if (parentId === undefined) {
      const newList = [...props.comments]
      const filter = newList.filter((x) => x.comId !== id)
      props.setComment(filter)
    } else if (parentId !== undefined) {
      const newList = [...props.comments]
      const index = newList.findIndex((x) => x.comId === parentId)
      const filter = newList[index].replies.filter((x) => x.comId !== id)
      newList[index].replies = filter
      props.setComment(newList)
    }
  }
  /* Submits replied or edited comment */
  const submit = (
    cancellor:any,
    text:string,
    parentId:string,
    edit:boolean,
    setText:Function,
    child:any
  ) => {
    if (edit) {
      editText(cancellor, text, parentId)
      handleCancel(cancellor, edit)
      setText('')
    } else {
      onSubmit(text, parentId, child)
      handleCancel(cancellor, false)
      setText('')
    }
  }

  return (
    <ActionContext.Provider
      value={{
        onSubmit: onSubmit,
        userImg: props.currentUser && props.currentUser.avatarUrl,
        userId: props.currentUser && props.currentUser.userId,
        handleAction: handleAction,
        handleCancel: handleCancel,
        replies: replies,
        setReplies: setReplies,
        editArr: editArr,
        onEdit: editText,
        onDelete: deleteText,
        signinUrl: props.signinUrl,
        signupUrl: props.signupUrl,
        user: user,
        submit: submit,

      }}
    >
      {props.children}
    </ActionContext.Provider>
  )
}