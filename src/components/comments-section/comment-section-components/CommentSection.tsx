import React, { useEffect, useState } from 'react'
import '../../../index.scss'
import DisplayComments from './DisplayComments'
import { ActionProvider } from './ActionContext'
import Input from './Input'
import IcommentSection from './../../../utils/interfaces/commentSection'

/* 
    //Wrapper for all comment section components
    props = {
      commentsArray: comment[];
      currentUser: user;
      setComment: Function;
      signinUrl: string;
      signupUrl: string;
    }
*/
export const CommentSection:React.FC<IcommentSection> = (props) => {
  const [comments, setComments] = useState(props.commentsArray);
  /* Updates commments */
  useEffect(() => {
    setComments(props.commentsArray)
  }, [props.commentsArray])

  return (
    <ActionProvider
      currentUser={props.currentUser}
      setComment={props.setComment}
      comments={comments}
      signinUrl={props.signinUrl}
      signupUrl={props.signupUrl}
    >
      <div className="section">
        <div className="inputBox">
          <Input />
        </div>
        <div className="displayComments">
          <DisplayComments comments={comments} />
        </div>
      </div>
    </ActionProvider>
  )
}
