import React, { useContext } from 'react';
import styles from '../../../index.scss';
import Popup from 'reactjs-popup';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faReply, faEllipsisV } from '@fortawesome/free-solid-svg-icons';
import { ActionContext } from './ActionContext';

const CommentStructure = ({ i, reply, parentId }) => {
  const actions = useContext(ActionContext)
  const edit = true

  return (
    <div className="halfDiv">
      <div
        className="userInfo"
        style={reply && { marginLeft: 15, marginTop: '6px' }}
      >
        <div>{i.text}</div>
        <div className="commentsTwo">
          <div>
            <img
              src={i.avatarUrl}
              style={{ width: 24, height: 24, borderRadius: 24 / 2 }}
              alt='userIcon'
            />
          </div>
          <div className="fullName">{i.fullName} </div>
          <div>
            <button
              className="replyBtn"
              onClick={() => actions.handleAction(i.comId)}
              disabled={!actions.user}
            >
              {' '}
              <FontAwesomeIcon icon={faReply} size='1x' color='#a5a5a5' /> Reply
            </button>
          </div>
        </div>
      </div>
      <div className="userActions">
        {actions.userId === i.userId && actions.user && (
          <Popup
            role='tooltip'
            trigger={
              <button className="actionsBtn">
                <FontAwesomeIcon icon={faEllipsisV} size='1x' color='#b9b9b9' />
              </button>
            }
            position='right center'
            nested
          >
            <div className={styles.actionDiv}>
              <div>
                <button
                  className={styles.editBtn}
                  onClick={() => actions.handleAction(i.comId, edit)}
                >
                  {' '}
                  edit
                </button>
              </div>
              <div>
                <Popup
                  trigger={
                    <button className={styles.deleteBtn}> delete</button>
                  }
                  modal
                  nested
                >
                  {(close) => (
                    <div className='modal'>
                      <button
                        className='close modalClose'
                        onClick={close}
                      >
                        &times;
                      </button>
                      <div className='header modalHeader'>
                        {' '}
                        Delete Comment{' '}
                      </div>
                      <div className='content modalContent'>
                        {' '}
                        Delete your comment permanently?
                      </div>
                      <div className='actions modalActions'>
                        <button
                          className='button modalActionBtn'
                          onClick={() => {
                            actions.onDelete(i.comId, parentId)
                            close()
                          }}
                        >
                          Delete
                        </button>
                        <button
                          className='button modalDelBtn'
                          onClick={() => {
                            close()
                          }}
                        >
                          Cancel
                        </button>
                      </div>
                    </div>
                  )}
                </Popup>
              </div>
            </div>
          </Popup>
        )}
      </div>
    </div>
  )
}

export default CommentStructure
