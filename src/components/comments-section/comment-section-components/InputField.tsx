import React, { useContext, useState, useEffect } from 'react'
import { ActionContext } from './ActionContext'
import '../../../index.scss'
import 'bootstrap/dist/css/bootstrap.min.css';
import IinputFiled from './../../../utils/interfaces/inputField'

/* 
    //Input shared component for posting comments and replys
    props = {
      cancellor: any;
      parentId?: string;
      value?: string;
      edit?: boolean;
      main?: any;
      child?: any;
    }
*/
const InputField:React.FC<IinputFiled> = (props) => {
  const [text, setText] = useState('')

  const handleChange = (e:any) => {
    setText(e.target.value)
  }

  useEffect(() => {props.value &&
    setText(props.value)
  }, [props.value])

  const actions = useContext(ActionContext)
  return (
    <form
      className="form"
      style={
        !props.child && !props.edit && props.main === undefined
          ? { marginLeft: 36 }
          : { marginLeft: 8 }
      }
    >
      <div className="input-group input-group-lg">
        <div>
          <img
            src={actions.userImg}
            style={{ width: 38, height: 38, borderRadius: 38 / 2, marginRight: 10, marginTop: 15 }}
            alt='userIcon'
          />
        </div>
        <input
          className= "postComment"
          type='text'
          placeholder='Type your reply here.'
          value={text}
          onChange={handleChange}
          aria-label="Large"
          aria-describedby="inputGroup-sizing-sm"
        />
      </div>
      <div className= "inputActions">
        <button
          className="postBtn"
          onClick={() =>{
              if (actions.submit) {
                if (props.edit) {
                  actions.submit(props.cancellor, text, props.parentId, true, setText)
                } else {
                  actions.submit(props.cancellor, text, props.parentId, false, setText)
                }
              }
            }
          }
          type='button'
          disabled={!text}
          style={
            !text
              ? { backgroundColor: '#84dcff' }
              : { backgroundColor: '#30c3fd' }
          }
        >
          Post
        </button>
        {(text || props.parentId) && (
          <button
            className="cancelBtn btn-danger"
            onClick={() =>{
              if (actions.handleCancel) {
                if (props.edit) {
                  actions.handleCancel(props.cancellor, props.edit)
                } else {
                  actions.handleCancel(props.cancellor)
                }
              }
            }
          }
            >
            Cancel
          </button>
        )}
      </div>
    </form>
  )
}

export default InputField