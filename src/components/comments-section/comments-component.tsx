import React, { useState } from 'react'
import data from "../../data/data.json"
import { CommentSection } from './comment-section-components/CommentSection'
import "../../index.scss"

/* 
  Comment section component 
  (Higest parent in herachy of this component)
*/
const CommentsComponent = () => {
  const [comment, setComment] = useState(data)
  const userId = "01a"
  const avatarUrl = "https://ui-avatars.com/api/name=Riya&background=random"
  const name = "Aaron Acuña"
  const signinUrl = "/signin"
  const signupUrl = "/signup"
  let count = 0
  comment.forEach(i => { 
    count += 1;
    i.replies && i.replies.forEach(i => count += 1) 
  })

  return(
    <>
      <div className="commentSection">
        <div className="header">Crocodiles are cool!</div>
        <CommentSection
          currentUser={userId && { userId: userId, avatarUrl: avatarUrl, name: name }}
          commentsArray={comment}
          setComment={setComment} signinUrl={signinUrl} signupUrl={signupUrl}
        />
      </div>
      <div className="verticalLine">
      </div>
   </>
  )
}

export default CommentsComponent;
