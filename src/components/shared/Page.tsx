import React, { memo } from 'react';
import { Container, Row } from 'react-bootstrap';
import Post from '../../pages/Post';

/* Canvas pasge functional component*/
const Page:React.FC = memo(() =>{
    const title = "Animals"
    const topic = "Crocodiles"
    return(
        <Container
            fluid
            className="my-4"
            style={{ paddingLeft: '1em', paddingRight: '1em' }}
        >
            <h1 className="h4">
            {title}
            </h1>
            <hr />
            <Row>
                <Post topic = {topic} />
            </Row>
        </Container>
    )
})
export default Page;