import React from 'react'
import CommentsComponent from '../components/comments-section/comments-component';
import Ipost from '../utils/interfaces/post'

/* 
    //Post page functional component
    props = {
        topic:string
    }
*/
const Post:React.FC<Ipost> = (props) => {
   
    return (
        <>
            <h2>Today's topic is: <b>{props.topic}</b></h2>
            <hr></hr>
            <CommentsComponent />
        </>
    );
  }
  
  export default Post;