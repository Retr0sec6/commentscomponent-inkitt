import replies from "./replies";

export default interface actionContext {
    onSubmit: Function;
    userImg: string;
    userId: string;
    handleAction?: Function;
    handleCancel: Function;
    replies: replies[];
    setReplies: Function;
    editArr: any[];
    onEdit: Function;
    onDelete: Function;
    signinUrl: string;
    signupUrl: string;
    user: boolean; //user?
    submit: Function;
}

