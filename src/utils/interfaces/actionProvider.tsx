import user from './user'
import comments from './comment'
export default interface actionProvider {
    currentUser: user;
    setComment: Function;
    comments: comments[];
    signinUrl: string;
    signupUrl: string;
}