import replies from './replies'
export default interface comment {
    userId: string;
    comId: string;
    fullName: string;
    avatarUrl: string;
    text: string;
    replies: replies[];
}