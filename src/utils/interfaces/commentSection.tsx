import comment from './comment'
import user from './user'
export default interface commentSection {
    commentsArray: comment[];
    currentUser: user;
    setComment: Function;
    signinUrl: string;
    signupUrl: string;
}