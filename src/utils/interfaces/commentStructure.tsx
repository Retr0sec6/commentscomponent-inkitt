import comment from "./comment";
import reply from "./replies";

export default interface commentStructure {
    comment:comment
    reply?:reply;
    parentId?: string;
    handleEdit?: Function;
}
