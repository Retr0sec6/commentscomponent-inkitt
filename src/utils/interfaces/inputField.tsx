export default interface inputField {
    cancellor: any;
    parentId?: string;
    value?: string;
    edit?: boolean;
    main?: any;
    child?: any;

}
