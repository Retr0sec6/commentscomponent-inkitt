export default interface replies {
    userId: string;
    comId: string;
    fullName: string;
    avatarUrl: string;
    text: string;
}