export default interface user {
    avatarUrl: string;
    name: string;
    userId: string;
}